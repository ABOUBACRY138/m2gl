# Notre projet s'appelle M2GL

Comme outil de travail nous avons utilsé comme IDE ECLIPSE, comme JDK le 1.8 et XAMPP comme serveur de base de donnée.

Etant un projet  maven, ci-dessous les dépendance qu'on a eu à installer  dans le ficher pom.xml

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>sn.isi</groupId>
  <artifactId>securityweb</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <packaging>war</packaging>
  <name>securityweb</name>
  	<dependencies>
	 	<!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-core -->
		<dependency>
		    <groupId>javax.servlet</groupId>
		    <artifactId>javax.servlet-api</artifactId>
		    <version>3.1.0</version>
	    </dependency>
		<dependency>
		    <groupId>org.hibernate</groupId>
		    <artifactId>hibernate-core</artifactId>
		    <version>5.4.32.Final</version>
		</dependency>
		<dependency>
		    <groupId>mysql</groupId>
		    <artifactId>mysql-connector-java</artifactId>
		    <version>8.0.28</version>
		</dependency>
	</dependencies>
	
  	<build>
    <finalName>securityweb</finalName>
    <pluginManagement><!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
      <plugins>
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>3.1.0</version>
        </plugin>
        <!-- see http://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_war_packaging -->
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.8.0</version>
          <configuration>
			  <source>1.8</source>
              <target>1.8</target>
          </configuration>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.22.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-war-plugin</artifactId>
          <version>3.2.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>2.5.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>2.8.2</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
</project>

Dans le fichier web.xml,
on a juste indexé le fichier login.jsp pour que cela s'affiche des le démarrage du serveur

<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://xmlns.jcp.org/xml/ns/javaee" xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd" id="WebApp_ID" version="4.0">
  <display-name>securityweb</display-name>
  <welcome-file-list>
    <welcome-file>login.jsp</welcome-file>
  </welcome-file-list>
</web-app>

La partie metier de notre application se déroule comme si:
- le package sn.isi.entities contient la création de nos entités compte et droit.
- le package sn.isi.dao contient les interfaces et les classe d'implémentation des entités.
- le package sn.isi.controller contient nos servlets (les controllers)
- le package sn.isi.DTO peut-être défini comme le double du package sn.isi.entities. on l'utilise pour ne pas avoir à toucher au package sn.isi.entities. Toucher ce dernier renvoit à toucher à la base de donnée et cela n'est pas une bonne pratique.
- le package sn.isi.util  contient le fichier hibernateUtil qui va nous permettre la liaison de notre appli à la base de donnée.
- le package sn.isi.service  





La partie présentation de notre application contient principalement le dossier webapp qui à son tour stocke les views chargés par les servlets.

//image webapp

Nous avons aussi fait quelques captures d'écran de notre application

- La page de connexion

//image con
- Le tableau de bord

// tableau bord

- Le formulaire d'ajout d'un compte

// image ajout compte

- La liste des comptes

// image liste compte

- Le formulaire d'ajout d'un droit

// image ajout droit

- La liste des droits

// image liste droit

Lien des captures d'écran
https://docs.google.com/presentation/d/1mZ7JBl8VvMFpVM4xATIxph-QKYk-YFWJ7YKmN-_ontI/edit#slide=id.g1fc8ae3d9c8_0_26
